package com.example.eventbuspatternse;

class CustomerMessageEvent {
    private String customMessage;

    public String getCustomMessage() {
        return customMessage;
    }

    public void setCustomMessage(String customMessage) {
        this.customMessage = customMessage;
    }
}
